from django import forms

class RegForm(forms.Form):
    name = forms.CharField(label = "姓名")
    nickname = forms.CharField(label = "昵称")
    passwd = forms.CharField(label = "密码")
    sex = forms.BooleanField(label = "性别")
    birth = forms.DateField(label = "出生日期")
    label = forms.CharField(max_length=50,label = "标签")
    phone = forms.CharField(max_length=15,label = "电话")
    email = forms.EmailField(label = "邮箱")
    pic = forms.CharField(max_length=100,label = "图像")


class LoginForm(forms.Form):
    name = forms.CharField(label="姓名")
    passwd = forms.CharField(label="密码")


class ArticleForm(forms.Form):
    title = forms.CharField(label="标题")
    content = forms.Textarea()
    cls = forms.CharField(label="分类")
    label = forms.CharField(label="标签")