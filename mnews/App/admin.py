from django.contrib import admin
from .models import Article
from .models import Commont
from .models import Userinfo
# Register your models here.
class ArticleAdmin(admin.ModelAdmin):
    list_display = ("title","author","time","content","browsenum","commontnum","cls")

class UserinfoAdmin(admin.ModelAdmin):
    list_display = ("name","nickname","sex","birth","policy","starnum","phone")

class CommontAdmin(admin.ModelAdmin):
    list_display = ("author","time","content","artical")

admin.site.register(Article,ArticleAdmin)
admin.site.register(Userinfo,UserinfoAdmin)
admin.site.register(Commont,CommontAdmin)