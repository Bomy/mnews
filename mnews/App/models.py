from django.db import models

# Create your models here.
class Userinfo(models.Model):
    name = models.CharField(max_length=30,verbose_name = "姓名")
    nickname = models.CharField(max_length=30,verbose_name = "昵称")
    passwd = models.CharField(max_length=100,verbose_name = "密码")
    sex = models.BooleanField(verbose_name = "性别",default="1")
    birth = models.DateField(verbose_name = "出生日期")
    signuptime = models.DateTimeField(verbose_name = "注册时间")
    policy = models.CharField(max_length=50,verbose_name = "权限",default='-1')
    level = models.IntegerField(verbose_name = "等级",default=0)
    starnum = models.IntegerField(verbose_name = "关注数",default=0)
    label = models.CharField(max_length=50,verbose_name = "标签",default="")
    newsnum = models.IntegerField(verbose_name = "文章数",default=0)
    phone = models.CharField(max_length=15,verbose_name = "电话")
    email = models.EmailField(verbose_name = "邮箱")
    lastlogintime = models.DateTimeField(verbose_name = "最近登录时间")
    lastlogintime = models.CharField(max_length=20,verbose_name="最近登录地点")
    remenber = models.BooleanField(verbose_name = "记忆登陆状态",default=False)
    pic = models.CharField(max_length=100,verbose_name = "图像")
    islogin = models.BooleanField(verbose_name = "是否登陆",default=False)


class Article(models.Model):
    title = models.CharField(max_length=50,verbose_name = "标题")
    author = models.CharField(max_length=50,verbose_name = "作者")
    time = models.DateTimeField(verbose_name = "时间")
    content = models.TextField(verbose_name = "内容")
    starsnum = models.IntegerField(verbose_name = "点赞数",default=0)
    browsenum = models.IntegerField(verbose_name = "浏览量",default=0)
    commontnum = models.IntegerField(verbose_name = "评论数",default=0)
    cls = models.CharField(max_length=50,verbose_name = "分类")
    label = models.CharField(max_length=50,verbose_name = "标签")


class Commont(models.Model):
    author = models.CharField(max_length=50,verbose_name = "评论人")
    time = models.DateTimeField(verbose_name = "时间")
    content = models.TextField(verbose_name = "内容")
    artical = models.IntegerField(verbose_name = "文章")

